# GameDev_WiSe2020

# AUGFEST 
## Introduction 
This is an augmented reality (AR) shooting game.  Objects spawn over the screen as soon as camera detects the plane. Player is supposed to shoot the objects within specific period of time and earn points to proceed to next level of the game. Game consists of 4 stages, players can earn/collect a max of 3 starts at the end of each stage. A total of 12 stars have to be collected through all these 4 stages to earn a gift at the end of the game. To keep players interest through the stages, small gifts are awarded after a player attain 4 and 8 stars at different stages of the game.

## Trailer  
https://youtu.be/TxWhfMKk9ns

## Development Dependencies
1) Unity 2019.4.16f1
2) AR Foundation v 2.1.4
3) AR CORE XR plugin 2.1.2 (For Android)
4) ARKIT XR Plugin 2.1.2 (For IOS)

## Installation Guide
1) Download the APK you want to install.
2) Navigate to your phone settings menu then to the security settings. Enable the Install from Unknown Sources option.
3) Use a file browser and navigate to your download folder. ..
4) The app should safely install.

## Folder Structure

### Build
The build folder is for your final delivery of the build version of your game. You will need this folder at the end of the seminar only. Please keep sure that everything is committed in the master branch and nothing is uncommitted by gitignore.

### Code
Here you can manage your code. Use different branches and tags to do so. However, by the end of the seminar the final version should be inside the master branch.

Please use gitignore to get rid of temporary and personal files.

### Doc
Inside the doc folder, the game design document (GDD), the slides, and videos have to be committed. This includes every single presentation, the preliminary GDD, and the final GDD. Like the build, everything should be found in the master branch.

