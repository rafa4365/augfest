﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftsShow : MonoBehaviour
{
    public GameObject gift1;
    public GameObject gift2;
    public GameObject gift3;

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<TimerLevel020304>().stopSpawn)
        {
            if (StarsAdd.starsCounter > 5)
            {
                gift1.SetActive(true);

            }
            if (StarsAdd.starsCounter > 8)
            {
                gift2.SetActive(true);
            }
            if (StarsAdd.starsCounter > 11)
            {
                gift3.SetActive(true);
            }
        }
    }
}
