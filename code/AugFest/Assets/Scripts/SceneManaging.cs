﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManaging : MonoBehaviour
{

    public void ChangeScene(int sceneIndex)
    {
        Debug.Log(" scene change ....   ");
        ScoreCounter.scoreValue = 0;
        SceneManager.LoadScene(sceneIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

