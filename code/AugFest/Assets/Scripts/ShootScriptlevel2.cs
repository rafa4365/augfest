﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ShootScriptlevel2 : MonoBehaviour
{

    public GameObject arCamera;
    public GameObject smoke;

// This is the shoot script for level2 in which the critical object which not to be shot is defined as balloon 3( Yellow Balloon)//
//If you shoot the critical balloon then you looose 30 points on eaxh shot encountered//
//Level2 also depends on a faster speed level along with randomization of balloons.//

    public void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {
            if (hit.transform.name == "balloon1(Clone)" || hit.transform.name == "balloon2(Clone)"||hit.transform.name == "balloon3(Clone)" )
            {
                Destroy(hit.transform.gameObject);
                ScoreCounter.scoreValue += 10;
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal)); 
            }

            if(hit.transform.name == "buttonNext")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                 SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
            }
            if(hit.transform.name == "buttonRetry")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                 SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        
            

        }
    }
}