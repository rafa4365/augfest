﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonScriptLevel3 : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * 1.0f);  // The Velocity of balloons in Level3 is 1.0f//
    }
}
