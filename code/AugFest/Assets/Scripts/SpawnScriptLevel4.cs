﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpawnScriptLevel4:MonoBehaviour    
{
    public Transform[] spawnPoints;
    public GameObject[] baloons;
    public Vector3 random;
    private Coroutine co;

    // Start is called before the first frame update
    void Start()
    {
       co = StartCoroutine(StartSpawning());
    }
    

    IEnumerator StartSpawning()
    {

        yield return new WaitForSeconds(3);

        for (int i = 0; i < 7; i++)
        {

            random = new Vector3(Random.Range(0f, 3f), 0, Random.Range(0f, 3f));  //  This code chunk is for  random spawning of all Game Objects(non critical as well as critical Game Objects//)

            Instantiate(baloons[i], spawnPoints[i].position + random, Quaternion.Euler(0, 180f, 0));    
            baloons[i].SetActive(true);
            //For the purpose of level3 and level4 as the critical objects are introduced in these levels so this line of code a making the rotation for the face of the bad monster as well as slime rabbit//

          
        }

        if(gameObject.GetComponent<TimerLevel020304>().stopSpawn)
        {
            StopCoroutine(); 
        }
        else
        {
            StartCoroutine(StartSpawning()); 
        }
        
    }
    
    public void StopCoroutine()
    {
        StopCoroutine(co);  
    }

}
