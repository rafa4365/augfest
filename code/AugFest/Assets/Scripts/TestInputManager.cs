﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class TestInputManager : MonoBehaviour
{
    public GameObject AR_object;
    public Camera AR_Camera;
    public GameObject ARcamera;
    public ARRaycastManager raycastManager;
    public List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private bool isTrue = true;
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isTrue)
        {
            isTrue = false;
            Ray ray = AR_Camera.ScreenPointToRay(Input.mousePosition);
            if (raycastManager.Raycast(ray, hits))
            {
                Pose pose = hits[0].pose;
                Instantiate(AR_object, pose.position + new Vector3(0f,8f,0f), pose.rotation);
                AR_object.GetComponentInChildren<levelSelction>().arCamera = ARcamera;
                //AR_object.SetActive(true);
            }
        }

    }
}