﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Timer : MonoBehaviour
{
    public TMP_Text Timertext;
    private float currentTime=0f;
    private float startTime = 60f;
    public int nextSceneLoad;
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;
    public GameObject endCanvas;
    public GameObject MainCanvas;
    public GameObject retryButton;
    public GameObject nextButton;
    

   
   
    public bool stickerCheck = true;
    public bool stopSpawn = false;
    
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startTime;
        nextSceneLoad = SceneManager.GetActiveScene().buildIndex + 1;
        //nextSceneLoad = 1;
    }

    public void nextStage()
    {
        SceneManager.LoadScene(nextSceneLoad);

        if (nextSceneLoad > PlayerPrefs.GetInt("LevelAt"))
        {
            PlayerPrefs.SetInt("LevelAt", nextSceneLoad);
        }

    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        
        Timertext.text = currentTime.ToString("0");
        if (currentTime <= 0)
        {
            currentTime = 0;
            if(ScoreCounter.scoreValue< 30)
            {
                SceneManager.LoadScene(2); //GameOver
            }
            else if(stickerCheck)
            {
                stickerCheck = false;

                if(ScoreCounter.scoreValue >49)
                {
                    StarsAdd.starsCounter += 1;
                    star1.SetActive(true);
                    ScoreCounter.StickersCollected +=1;

                    if(ScoreCounter.scoreValue >99)
                    {
                        StarsAdd.starsCounter += 1;
                        star2.SetActive(true);
                        ScoreCounter.StickersCollected +=1;

                        if(ScoreCounter.scoreValue >199)
                        {
                            StarsAdd.starsCounter += 1;
                            star3.SetActive(true);
                        }
                        
                    }
                    //MainCanvas.SetActive(false);
                    endCanvas.SetActive(true);
                    retryButton.SetActive(true);
                    nextButton.SetActive(true);
                    //gameObject.GetComponent<SpawnScriptLevel1>().enabled = false;
                    stopSpawn = true;
                   
                }

            }
            
        }
    }






}
