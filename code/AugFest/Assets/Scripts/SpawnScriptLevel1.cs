using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScriptLevel1 : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] baloons;
    private  Coroutine co;

    // Start is called before the first frame update
    void Start()
    {
        co = StartCoroutine(StartSpawning());
        
    }

    IEnumerator StartSpawning()
    {

        yield return new WaitForSeconds(3);

        for (int i = 0; i < 3; i++)
        {
            Instantiate(baloons[i], spawnPoints[i].position, Quaternion.identity);
            baloons[i].SetActive(true);
        }

        if(gameObject.GetComponent<Timer>().stopSpawn)
        {
            StopCoroutine();
        }
        else
        {
            StartCoroutine(StartSpawning());
        }

    }

    public void StopCoroutine()
    {
        StopCoroutine(co);
    }



}
