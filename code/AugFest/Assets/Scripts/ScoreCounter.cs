﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreCounter : MonoBehaviour
{
    public static int scoreValue = 0;
    public static int StickersCollected = 0;
    //Text Score;
    private TMP_Text Score;
    // Start is called before the first frame update
    public TMP_Text Stickers;

    void Start()
    {
        Score = GetComponent<TextMeshProUGUI>() ?? gameObject.AddComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        Score.text =  "  SCORE: "+scoreValue.ToString();
        
    }
}
