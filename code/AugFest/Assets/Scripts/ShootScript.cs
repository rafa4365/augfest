﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootScript : MonoBehaviour
{

    public GameObject arCamera;
    public GameObject smoke;

    public void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {
            if (hit.transform.name == "balloon1(Clone)" || hit.transform.name == "balloon2(Clone)" ||hit.transform.name == "balloon3(Clone)") 
            {
                Destroy(hit.transform.gameObject);
                ScoreCounter.scoreValue += 10;
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));
            }
            else if (hit.transform.name == "buttonNext")
            {
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else if (hit.transform.name == "buttonRetry")
            {
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            /*
            else if (hit.transform.name == "balloon3(Clone)")
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue -20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));

            }
           //else if (hit.transform.name == "SlimeRabbit_Prefab(Clone)")  // good boy//
          //  {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue - 20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));//

            //}

           // else if (hit.transform.name == "Monster_Bat_Prefab(Clone)")
           // {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue + 20; //bad boy//
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));    //

           // }*/

        }
    }
}
