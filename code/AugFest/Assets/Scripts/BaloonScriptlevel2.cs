using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonScriptLevel2 : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * 0.8f);  // The Velocity of balloons in Level2 is 0.8f(float)
    }
}
