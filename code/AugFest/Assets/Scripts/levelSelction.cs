﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelSelction : MonoBehaviour
{

    public GameObject arCamera;
    public GameObject smoke;

    private void Start()
    {
        arCamera = GameObject.Find("AR Camera");        
    }
    public void Shoot()
        {
            RaycastHit hit;

            if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
            {
                if (hit.transform.name == "1") 
                {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                    SceneManager.LoadScene(2);

                }

                if(hit.transform.name == "2")
                {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                    SceneManager.LoadScene(3);
                }

                if(hit.transform.name == "3")
                {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                    SceneManager.LoadScene(4);
                }

                if(hit.transform.name == "4")
                {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                    SceneManager.LoadScene(5);
                }
            if (hit.transform.name == "Play")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(1);

            }
            if (hit.transform.name == "Quit")
            {
                //Destroy(hit.transform.gameObject);
                ScoreCounter.scoreValue = 0;
                Application.Quit();

            }

        }


        }
    
}
