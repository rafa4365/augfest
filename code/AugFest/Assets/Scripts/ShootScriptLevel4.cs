﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootScriptLevel4 : MonoBehaviour
{

    public GameObject arCamera;
    public GameObject smoke;
    

// This is the shoot script for level4 which contains a total of five Game Shooting and non shooting Targets//
//Balloon1   Blue Color                       //
//Balloon2   Red  Color                       //
//Balloon3   Yellow Color    Critical Object1 //
//Monster_Bat(Bad boy)       Critical bonus Oject                //  
//SlimeRabbit(Good Boy)      Critical Object2 // 


    public void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {
            if (hit.transform.name == "buttonRetry")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            if (hit.transform.name == "balloon1(Clone)" || hit.transform.name == "balloon2(Clone)") 
            {
                Destroy(hit.transform.gameObject);
                ScoreCounter.scoreValue += 10;
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));
            }
            
            else if (hit.transform.name == "balloon3(Clone)")  // Critical Balloon//
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue -20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));

            }
            else if (hit.transform.name == "SlimeRabbit_Prefab(Clone)")  // Good boy//
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue - 20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));//

            }

            else if (hit.transform.name == "Monster_Bat_Prefab(Clone)")
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue + 20;   //Bad boy//
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));    

            }

        }
    }
    
}
