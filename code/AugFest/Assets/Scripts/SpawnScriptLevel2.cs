﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScriptLevel2:MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] baloons;
    public Vector3 random;
    private Coroutine co;  // Updated from timer 01//

    // Start is called before the first frame update
    void Start()
    {
        co = StartCoroutine(StartSpawning());
    }

    IEnumerator StartSpawning()
    {

        yield return new WaitForSeconds(3);

        for (int i = 0; i < 5; i++)
        {
            random = new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f));  // In this level#2 the number of balloons will be more but the position is randomized and the speed is made little faster//
          //  Instantiate(baloons[i], spawnPoints[i].position + random, Quaternion.Euler(0, 180f, 0));    //  for he purpose of level 3//

          Instantiate(baloons[i], spawnPoints[i].position + random, Quaternion.identity);
          baloons[i].SetActive(true);
        
        }
        if(gameObject.GetComponent<TimerLevel020304>().stopSpawn)
        {
            StopCoroutine();
        }
        else
        {
            StartCoroutine(StartSpawning());
        }
    
    }
    
    public void StopCoroutine()  
    {
        StopCoroutine(co);
    }

}
