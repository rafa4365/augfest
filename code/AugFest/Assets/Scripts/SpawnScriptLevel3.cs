﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpawnScriptLevel3:MonoBehaviour

{
    public Transform[] spawnPoints;
    public GameObject[] baloons;
    public Vector3 random;
    private Coroutine co;    // added line from sSpawnScript02//


    void Start()
    {
        co = StartCoroutine(StartSpawning());
    }


    IEnumerator StartSpawning()
    {

        yield return new WaitForSeconds(3);

        for (int i = 0; i < 5; i++)
        {

            random = new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f));  //  This code chunk is for  random spawning of all Game Objects(non critical as well as critical Game Objects//)

            Instantiate(baloons[i], spawnPoints[i].position + random, Quaternion.Euler(0, 180f, 0));    
            baloons[i].SetActive(true);
            //for the purpose of level3 and the euler making the rotation for the face of the bad monster as well as slime rabbit//

          
        }

        if(gameObject.GetComponent<TimerLevel020304>().stopSpawn)
        {
            StopCoroutine();
        }
        else
        {
            StartCoroutine(StartSpawning());
        } 

    }

    public void StopCoroutine()  
    {
        StopCoroutine(co);
    }

}
