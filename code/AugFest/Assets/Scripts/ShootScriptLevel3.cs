﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootScriptLevel3 : MonoBehaviour
{

    public GameObject arCamera;
    public GameObject smoke;
    

    // This is the shoot script for level3 which contains a total of five Game Shooting and non shooting Targets//
    //Balloon1   Blue Color                       //       if you shoot it you get +10 points//
    //Balloon2   Red  Color                       //       if you shoot it you get +10 points//
    //Balloon3   Yellow Color    Critical Object1 //       if you shoot it you get -20 points//
    //Monster_Bat(Bad boy)       Critical bonus object  // if you shoot it you get +20 points//
    //SlimeRabbit(Good Boy)      Critical  Object //       if you shoot it you get -20 points//


    public void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))
        {
            if (hit.transform.name == "buttonNext")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            if (hit.transform.name == "buttonRetry")
            {
                ScoreCounter.scoreValue = 0;
                Destroy(hit.transform.gameObject);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            if (hit.transform.name == "balloon1(Clone)" || hit.transform.name == "balloon2(Clone)") 
            {
                Destroy(hit.transform.gameObject);
                ScoreCounter.scoreValue += 10;
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));
            }
            
            else if (hit.transform.name == "balloon3(Clone)") // Critical balloon//
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue -20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));

            }
            else if (hit.transform.name == "SlimeRabbit_Prefab(Clone)")  // Good boy//
           {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue - 20;
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));//

            }

            else if (hit.transform.name == "Monster_Bat_Prefab(Clone)")
            {
                ScoreCounter.scoreValue = ScoreCounter.scoreValue + 20;   //Bad boy//
                Destroy(hit.transform.gameObject);
                Instantiate(smoke, hit.point, Quaternion.LookRotation(hit.normal));    

            }


        }
    }
}
